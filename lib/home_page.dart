
// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:v1/utils/app_colors.dart';




void main() => runApp(const GetMaterialApp(home: HomeView()));

// ignore: camel_case_types

class HomeView extends StatelessWidget {
  const HomeView({super.key});
  static const appTitle = "Task 2";

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: appTitle,
      home: MyHomePage(title: appTitle),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  @override
  State<MyHomePage> createState() => _MyHomePage();
  final String title;
}

class _MyHomePage extends State<MyHomePage> {
  static final List<Widget> _widgetOption = <Widget>[
    const Text('data'),
    const Text('data'),
    const Text('data'),
    const Text('data'),
  ];
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(         
      backgroundColor: AppColors.darkBackground,
      bottomNavigationBar: BottomNavigationBar(backgroundColor: AppColors.darkFlatGold,
        items:  <BottomNavigationBarItem>[
           BottomNavigationBarItem(
            icon: const Icon(Icons.home),backgroundColor: AppColors.darkBackground,
            label: 'Home',
          ),
           BottomNavigationBarItem(
            icon: const Icon(Icons.business),backgroundColor: AppColors.darkFlatGold,
            label: 'Business',
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.school),backgroundColor: AppColors.darkFlatGold,
            label: 'School',
          ),
        ],
      ),
      /*drawer: Drawer(backgroundColor: AppColors.darkBackground,
      // add list view di dalam drawer
      //through the option if isn't enough for vertical
      //space to fit everything.
      child: ListView(
        //remove any padding in listview
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(decoration: BoxDecoration(color: Color.fromARGB(255, 214, 190, 103)),
          child: Text('Menu'),
          ),
          ListTile(textColor: AppColors.darkGold,
            title: const Text('Item 1'),onTap: () {
            },
          ),
          ListTile(textColor: AppColors.darkGold,
            title: const Text('Item 2'),onTap: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
    ),*/
     
      body: SafeArea(
        child: Center(
          child: Column(
            children: <Widget>[
              Image.asset('assets/images/black_owl_large.png',
                  width: 275, height: 75),
         
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 0, bottom: 10),
                    child: SizedBox(
                      width: 175,
                      height: 50,
                      child: TextButton.icon(
                        label: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Juan Bruen\n',
                                style: TextStyle(
                                    color: AppColors.whiteSkeleton, fontSize: 14),
                              ),
                              TextSpan(
                                text: 'VVIP',
                                style: TextStyle(
                                    color: AppColors.darkGold,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {},
                        style: TextButton.styleFrom(
                            backgroundColor: AppColors.darkBackground),
                        icon: CircleAvatar(
                          backgroundImage:
                              const AssetImage('assets/icons/profile.png'),
                          backgroundColor: AppColors.darkBackground,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 175),
                    child: Icon(
                      Icons.notifications,
                      color: AppColors.greyDisabled,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 23),
                child: Row(
                  children: [
                    SizedBox(
                      width: 175,
                      height: 50,
                      child: ElevatedButton.icon(
                        label: Text(
                          ' Voucher\n Rp.735.000,-',
                          style: TextStyle(
                            color: AppColors.darkBackground,
                          ),
                        ),
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          backgroundColor: AppColors.darkFlatGold,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                            ),
                          ),
                        ),
                        icon: Image.asset(
                          'assets/icons/voucher.png',
                          width: 20,
                          height: 20,
                          color: AppColors.darkBackground,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 175,
                      height: 50,
                      child: ElevatedButton.icon(
                        label: Text(
                          ' Point\n 500.000 pts',
                          style: TextStyle(
                            color: AppColors.darkBackground,
                          ),
                        ),
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          backgroundColor: AppColors.darkFlatGold,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                          ),
                        ),
                        icon: Image.asset(
                          'assets/icons/point.png',
                          width: 20,
                          height: 20,
                          color: AppColors.darkBackground,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 350,
                  height: 50,
                  child: OutlinedButton.icon(
                    label: Text(
                      'My E-Tikcet',
                      style: TextStyle(color: AppColors.darkGold),
                    ),
                    icon: Image.asset(
                      'assets/icons/voucher.png',
                      width: 20,
                      height: 20,
                      color: AppColors.darkGold,
                    ),
                    onPressed: () {},
                    style: OutlinedButton.styleFrom(
                      side: BorderSide(width: 2, color: AppColors.darkFlatGold),
                      backgroundColor: AppColors.darkBackground,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: SizedBox(
                  child: Center(
                    child: ListView(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(bottom: 50),
                        ),
                        SizedBox(
                          height: 150,
                          child: ListView(
                            physics: const BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(
                                  left: 20,
                                ),
                                width: 300,
                                height: 200,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.black12,
                                  image: const DecorationImage(
                                    image: NetworkImage(
                                      "https://images.unsplash.com/photo-1607355739828-0bf365440db5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1444&q=80",
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                  left: 20,
                                ),
                                width: 300,
                                height: 200,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  image: const DecorationImage(
                                    image: NetworkImage(
                                      "https://images.pexels.com/photos/2583852/pexels-photo-2583852.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260",
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                  left: 20,
                                ),
                                width: 300,
                                height: 200,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  image: const DecorationImage(
                                    image: NetworkImage(
                                      "https://images.unsplash.com/photo-1584810359583-96fc3448beaa?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80",
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );

    /*BottomNavigationBar(items: const <BottomNavigationBarItem>[BottomNavigationBarItem(icon: Icon(Icons.home),label: 'Home'),
    BottomNavigationBarItem(icon: Icon(Icons.picture_in_picture_alt_rounded),label: 'Picture'),
    BottomNavigationBarItem(icon: Icon(Icons.color_lens),label: 'Custom Paint')
    ],
    currentIndex: _selectedIndex,
    selectedItemColor: Color.fromARGB(255, 37, 36, 36),
    onTap: _onItemTapped,*/
  }
}
