import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/size_config.dart';
import 'frame_action.dart';
import 'frame_on_back_tittle.dart';

class FrameActionWrapper extends StatelessWidget {
  const FrameActionWrapper({
    required this.title,
    required this.onPressed,
    this.fontWeight,
    Key? key,
  }) : super(key: key);

  final String? title;
  final Function() onPressed;
  final FontWeight? fontWeight;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
      width: SizeConfig.screenWidth,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: FrameOnBackTitle(
              title: title!,
              onPressed: onPressed,
              fontWeight: fontWeight,
            ),
          ),
          const ResponsiveRowColumnItem(child: FrameAction()),
        ],
      ),
    );
  }
}
