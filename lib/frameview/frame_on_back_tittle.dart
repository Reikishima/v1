import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';
import '../text/montserrat_text_view.dart';

class FrameOnBackTitle extends StatelessWidget {
  const FrameOnBackTitle({
    required this.title,
    required this.onPressed,
    this.fontWeight,
    Key? key,
  }) : super(key: key);

  final String title;
  final Function() onPressed;
  final FontWeight? fontWeight;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SizedBox(width: SizeConfig.horizontal(2)),
        ),
        ResponsiveRowColumnItem(
          child: IconButton(
            onPressed: onPressed,
            iconSize: 10,
            icon: Image.asset(
              Assets.iconBack,
              color: AppColors.whiteBackground,
            ),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(
            width: SizeConfig.horizontal(2),
          ),
        ),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: title,
            fontWeight: fontWeight ?? FontWeight.w300,
            size: SizeConfig.safeHorizontal(5),
            color: AppColors.whiteBackground,
          ),
        ),
      ],
    );
  }
}
