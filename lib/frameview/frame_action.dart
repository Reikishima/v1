import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';

class FrameAction extends StatelessWidget {
  const FrameAction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Image.asset(
            Assets.imageBlackOwlTransparent,
            fit: BoxFit.cover,
            width: SizeConfig.horizontal(10),
            height: SizeConfig.horizontal(10),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(width: SizeConfig.horizontal(4)),
        ),
      ],
    );
  }
}
