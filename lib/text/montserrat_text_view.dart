import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:google_fonts/google_fonts.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';

class MontserratTextView extends StatelessWidget {
  // constructor
  const MontserratTextView({
    Key? key,
    required this.value,
    this.color,
    this.size,
    this.fontStyle,
    this.fontWeight,
    this.alignText,
    this.isUnderline = false,
    this.overflow,
    this.maxLines,
  }) : super(key: key);

  final String value;
  final Color? color;
  final double? size;
  final FontStyle? fontStyle;
  final FontWeight? fontWeight;
  final AlignTextType? alignText;
  final bool isUnderline;
  final TextOverflow? overflow;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Text(
      value,
      overflow: overflow,
      maxLines: maxLines,
      style: GoogleFonts.montserrat(
        color: color ?? AppColors.basicDark,
        fontSize: size ?? SizeConfig.safeHorizontal(3),
        fontStyle: fontStyle ?? FontStyle.normal,
        fontWeight: fontWeight,
        decoration: isUnderline ? TextDecoration.underline : null,
      ),
      textAlign: alignText == AlignTextType.center
          ? TextAlign.center
          : alignText == AlignTextType.right
              ? TextAlign.right
              : TextAlign.left,
    );
  }
}

class MontserratTextStyle {
  TextStyle whiteDisplay3() {
    return GoogleFonts.montserrat(
      color: AppColors.whiteBackground,
      fontSize: SizeConfig.safeHorizontal(5),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w300,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle white5([double? size]) {
    return GoogleFonts.montserrat(
      color: AppColors.whiteBackground,
      fontSize: SizeConfig.safeHorizontal(size ?? 5),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w300,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle grey5([double? size]) {
    return GoogleFonts.montserrat(
      color: AppColors.lightDark,
      fontSize: SizeConfig.safeHorizontal(size ?? 5),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w300,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle registerFieldWhite([double? size]) {
    return GoogleFonts.montserrat(
      color: AppColors.whiteBackground,
      fontSize: SizeConfig.safeHorizontal(size ?? 4),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w300,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle readOnlyGrey() {
    return GoogleFonts.montserrat(
      color: Colors.grey,
      fontSize: SizeConfig.safeHorizontal(4),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w300,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle registerFieldGrey([double? size]) {
    return GoogleFonts.montserrat(
      color: Colors.grey,
      fontSize: SizeConfig.safeHorizontal(size ?? 5),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w100,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle otpFieldGrey([double? size]) {
    return GoogleFonts.montserrat(
      color: Colors.grey,
      fontSize: SizeConfig.safeHorizontal(size ?? 5),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w300,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle greyCalendar() {
    return GoogleFonts.montserrat(
      color: AppColors.whiteBackground.withOpacity(0.3),
      fontSize: SizeConfig.safeHorizontal(4.5),
      fontWeight: FontWeight.w400,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle whiteTitleEvent6([double? size]) {
    return GoogleFonts.montserrat(
      color: AppColors.whiteBackground,
      fontSize: SizeConfig.safeHorizontal(size ?? 5),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w700,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle whiteDescriptionTitleEvent4() {
    return GoogleFonts.montserrat(
      color: AppColors.whiteBackground,
      fontSize: SizeConfig.safeHorizontal(3.5),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w700,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle whiteDescriptionEvent4() {
    return GoogleFonts.montserrat(
      color: AppColors.whiteBackground,
      fontSize: SizeConfig.safeHorizontal(3.5),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w300,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle flatGoldDescriptionPriceItem4([double? size]) {
    return GoogleFonts.montserrat(
      color: AppColors.flatGold,
      fontSize: SizeConfig.safeHorizontal(size ?? 4),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w700,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle flatGoldDescriptionPriceItem6([double? size]) {
    return GoogleFonts.montserrat(
      color: AppColors.flatGold,
      fontSize: SizeConfig.safeHorizontal(size ?? 7),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w700,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle black14px700() {
    return GoogleFonts.montserrat(
      color: AppColors.darkBackground,
      fontSize: SizeConfig.safeHorizontal(3),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w700,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle black14px300() {
    return GoogleFonts.montserrat(
      color: AppColors.darkBackground,
      fontSize: SizeConfig.safeHorizontal(3),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w300,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle black16px700() {
    return GoogleFonts.montserrat(
      color: AppColors.darkBackground,
      fontSize: SizeConfig.safeHorizontal(3.5),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w700,
      // decoration: TextDecoration.underline,
    );
  }

  TextStyle black24px700() {
    return GoogleFonts.montserrat(
      color: AppColors.darkBackground,
      fontSize: SizeConfig.safeHorizontal(8),
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.w700,
      // decoration: TextDecoration.underline,
    );
  }
}
