import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:google_fonts/google_fonts.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';

class MontserratTextError extends StatelessWidget {
  const MontserratTextError({
    Key? key,
    required this.value,
    this.size,
    this.fontStyle,
    this.fontWeight,
  }) : super(key: key);

  final String value;
  final double? size;
  final FontStyle? fontStyle;
  final FontWeight? fontWeight;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Text(
      value,
      style: GoogleFonts.montserrat(
        color: AppColors.redAlert,
        fontSize: size ?? SizeConfig.safeBlockHorizontal * 3,
        fontStyle: fontStyle ?? FontStyle.normal,
        fontWeight: fontWeight ?? FontWeight.w300,
      ),
    );
  }
}
