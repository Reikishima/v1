import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
// ignore: depend_on_referenced_packages
import 'package:responsive_framework/responsive_framework.dart';

import '../../../helpers/date_helper.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';

import '../layout/uner_lines.dart';
import '../text/montserrat_text_view.dart';
import 'custom_date_picker.dart';

class BirthDate extends StatelessWidget {
  const BirthDate({
    Key? key,
    required this.onDateTimeChange,
    required this.selectedDate,
    this.isReadOnly = false,
  }) : super(key: key);

  final Function(DateTime) onDateTimeChange;
  final DateTime? selectedDate;
  final bool isReadOnly;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'register_birth_date_form_label'.tr,
            color: AppColors.whiteBackground,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3),
          ),
        ),
        ResponsiveRowColumnItem(child: _birthDateField(context)),
        const ResponsiveRowColumnItem(child: Underline()),
      ],
    );
  }

  Widget _birthDateField(BuildContext context) {
    return Material(
      color: AppColors.darkBackground,
      child: InkWell(
        onTap: () => isReadOnly
            ? null
            : showCupertinoModalPopup(
                context: context,
                builder: (BuildContext context) {
                  final FocusScopeNode currentFocus = FocusScope.of(context);
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  return CustomDatePicker(
                    onDateTimeChange: onDateTimeChange,
                    selectedDate: selectedDate,
                  );
                },
              ),
        child: Container(
          margin: EdgeInsets.only(
            top: SizeConfig.vertical(2),
            bottom: SizeConfig.vertical(0.5),
          ),
          width: SizeConfig.horizontal(100),
          child: MontserratTextView(
            value: DateHelper.getBirthOfDateDisplayer(selectedDate.toString()),
            alignText: AlignTextType.left,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.5),
            color: isReadOnly ? AppColors.lightDark : AppColors.whiteBackground,
          ),
        ),
      ),
    );
  }
}
