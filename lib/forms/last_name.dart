import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
// ignore: depend_on_referenced_packages
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';

import '../layout/space_sizer.dart';
import '../text/monstserrat_text_error.dart';
import '../text/montserrat_text_view.dart';

class LastName extends StatelessWidget {
  const LastName({
    Key? key,
    required this.lastNameController,
    required this.errorText,
    this.hintText,
    this.isReadOnly = false,
  }) : super(key: key);

  final TextEditingController lastNameController;
  final String? hintText;
  final String errorText;
  final bool isReadOnly;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'register_last_name_form_label'.tr,
            color: AppColors.whiteBackground,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3),
          ),
        ),
        ResponsiveRowColumnItem(child: _lastNameField()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: MontserratTextError(value: errorText)),
      ],
    );
  }

  Widget _lastNameField() {
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return TextField(
      controller: lastNameController,
      cursorColor: AppColors.darkGold,
      keyboardType: TextInputType.name,
      textAlign: TextAlign.left,
      readOnly: isReadOnly,
      style: montserratStyle.registerFieldWhite(),
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.only(top: SizeConfig.vertical(2)),
        // border: InputBorder.none,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: AppColors.flatGold),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: AppColors.flatGold),
        ),
        hintText: hintText,
        hintStyle: montserratStyle.registerFieldGrey(),
      ),
    );
  }
}
