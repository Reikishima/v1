import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../text/montserrat_text_view.dart';

class CustomDatePicker extends StatelessWidget {
  const CustomDatePicker({
    Key? key,
    required this.onDateTimeChange,
    required this.selectedDate,
  }) : super(key: key);

  final Function(DateTime) onDateTimeChange;
  final DateTime? selectedDate;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return SizedBox(
      height: SizeConfig.vertical(30),
      child: CupertinoTheme(
        data: CupertinoThemeData(
          textTheme: CupertinoTextThemeData(
            dateTimePickerTextStyle: montserratStyle.white5(),
          ),
        ),
        child: Container(
          color: AppColors.darkBackground,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text(
                      'Done',
                      style: montserratStyle.registerFieldWhite(),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: CupertinoDatePicker(
                  backgroundColor: AppColors.darkBackground,
                  onDateTimeChanged: onDateTimeChange,
                  initialDateTime: selectedDate,
                  maximumYear: 5000,
                  mode: CupertinoDatePickerMode.date,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
