import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
// ignore: depend_on_referenced_packages
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../layout/space_sizer.dart';
import '../layout/uner_lines.dart';

import '../text/monstserrat_text_error.dart';

import '../text/montserrat_text_view.dart';

class Address extends StatelessWidget {
  const Address({
    Key? key,
    required this.addressController,
    required this.hintText,
    required this.errorText,
  }) : super(key: key);

  final TextEditingController addressController;
  final String hintText;
  final String errorText;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'register_address_form_label'.tr,
            color: AppColors.whiteBackground,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3),
          ),
        ),
        ResponsiveRowColumnItem(child: _yourAddressField()),
        const ResponsiveRowColumnItem(child: Underline()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: MontserratTextError(value: errorText)),
      ],
    );
  }

  Widget _yourAddressField() {
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: TextField(
            controller: addressController,
            cursorColor: AppColors.darkGold,
            maxLines: 2,
            keyboardType: TextInputType.name,
            textAlign: TextAlign.left,
            style: montserratStyle.registerFieldWhite(),
            decoration: InputDecoration(
              isDense: true,
              // contentPadding: EdgeInsets.only(top: SizeConfig.vertical(2)),
              border: InputBorder.none,
              hintText: hintText,
              hintStyle: montserratStyle.registerFieldGrey(),
            ),
          ),
        ),
      ],
    );
  }
}
