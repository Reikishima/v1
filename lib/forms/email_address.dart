import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../layout/space_sizer.dart';

import '../text/monstserrat_text_error.dart';

import '../text/montserrat_text_view.dart';

class EmailAddress extends StatelessWidget {
  const EmailAddress({
    Key? key,
    required this.emailAddressController,
    this.hintText,
    required this.errorText,
    required this.isReadOnly,
  }) : super(key: key);

  final TextEditingController emailAddressController;
  final String? hintText;
  final String errorText;
  final bool isReadOnly;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Email Address',
            color: Colors.white,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3),
          ),
        ),
        ResponsiveRowColumnItem(child: _emailAddressField()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: MontserratTextError(value: errorText)),
      ],
    );
  }

  Widget _emailAddressField() {
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return TextField(
      readOnly: isReadOnly,
      controller: emailAddressController,
      cursorColor: AppColors.darkGold,
      keyboardType: TextInputType.emailAddress,
      textAlign: TextAlign.left,
      style: montserratStyle.registerFieldWhite(),
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.only(top: SizeConfig.vertical(2)),
        // border: InputBorder.none,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: AppColors.flatGold),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: AppColors.flatGold),
        ),
        hintText: hintText,
        hintStyle: montserratStyle.registerFieldGrey(),
      ),
    );
  }
}
