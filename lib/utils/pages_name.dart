class PageNames {
  PageNames._();
  static String bottlePage = 'Bottle Packages';
  static String eventPage = "What's New";
  static String foodPage = 'Special Offers';
  static String pointsPage = 'Points History';
  static String registerPage = 'Register';
  static String voucherPage = 'Vouchers';
  static String galleryPage = 'Gallery';
  static String redeemPage = 'Redeem Points';
  static String profilePage = 'Profile';
  static String editProfilePage = 'Edit Personal Info';
  static String recentTransactionPage = 'Redeem History';
}
