import 'package:flutter/material.dart';

import 'frame_app_bar.dart';

class FrameScaffold extends StatelessWidget {
  // constructor
  const FrameScaffold({
    Key? key,
    // parameter for app bar
    this.titleScreen,
    this.heightBar,
    this.color,
    this.isCenter,
    this.elevation,
    this.isUseLeading,
    this.onBack,
    this.customLeading,
    this.action,
    this.isImplyLeading,
    this.customTitle,
    this.colorScaffold,
    // parameter for scaffold
    this.view,
    this.bottomSheet,
    this.avoidBottomInset,
    this.extendBodyBehindAppBar,
    // system status bar
    this.statusBarColor,
    this.statusBarIconBrightness,
    this.statusBarBrightness, 
  }) : super(key: key);

  final Widget? view;
  final Widget? bottomSheet;
  final bool? avoidBottomInset;
  final bool? extendBodyBehindAppBar;

  final String? titleScreen;
  final double? heightBar;
  final Color? color;
  final bool? isCenter;
  final double? elevation;
  final bool? isUseLeading;
  final dynamic Function()? onBack;
  final Widget? customLeading;
  final Widget? action;
  final bool? isImplyLeading;
  final Widget? customTitle;
  final Color? colorScaffold;

  // system status bar
  final Color? statusBarColor;
  final Brightness? statusBarIconBrightness;
  final Brightness? statusBarBrightness;


  @override
  Widget build(BuildContext context) {
    return _frameWidget();
  }

  Widget _frameWidget() {
    return Scaffold(
      resizeToAvoidBottomInset: avoidBottomInset ?? true,
      backgroundColor: colorScaffold,
      appBar: FrameAppBar(
        titleScreen: titleScreen,
        heightBar: heightBar,
        color: color,
        isCenter: isCenter,
        elevation: elevation,
        isUseLeading: isUseLeading,
        onBack: onBack,
        customLeading: customLeading,
        action: action,
        isImplyLeading: isImplyLeading,
        customTitle: customTitle,
        statusBarColor: statusBarColor,
        statusBarIconBrightness: statusBarIconBrightness,
        statusBarBrightness: statusBarBrightness,
      ),
      bottomSheet: bottomSheet,
      body: view,
      extendBodyBehindAppBar: extendBodyBehindAppBar ?? false,
    );
  }
}
