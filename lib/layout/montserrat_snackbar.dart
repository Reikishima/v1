import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../text/montserrat_text_view.dart';
import 'space_sizer.dart';

SnackBar montserratSnackbar({
  required bool status,
  required String title,
  required String message,
  Duration? duration,
  Color? customColor,
}) {
  return SnackBar(
    padding: EdgeInsets.symmetric(
      horizontal: SizeConfig.horizontal(2.5),
      vertical: SizeConfig.vertical(1.5),
    ),
    duration: duration ?? const Duration(seconds: 4),
    backgroundColor: customColor ??
        (status == true ? AppColors.greenInfo : AppColors.redAlert),
    content: ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: title,
            color: AppColors.whiteBackground,
            size: SizeConfig.safeHorizontal(4),
            fontWeight: FontWeight.bold,
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: message,
            color: AppColors.whiteBackground,
            size: SizeConfig.safeHorizontal(3.5),
            fontWeight: FontWeight.w300,
          ),
        ),
      ],
    ),
    behavior: SnackBarBehavior.floating,
  );
}
