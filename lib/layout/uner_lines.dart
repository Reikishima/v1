import 'package:flutter/material.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';

class Underline extends StatelessWidget {
  const Underline({this.lineColor, Key? key}) : super(key: key);

  final Color? lineColor;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      height: SizeConfig.vertical(0.2),
      width: SizeConfig.horizontal(1000),
      color: lineColor ?? AppColors.flatGold,
    );
  }
}
