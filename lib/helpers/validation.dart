import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

import '../layout/montserrat_snackbar.dart';
import 'regex_helper.dart';

class Validation {
  Validation._();

  static bool _isDate(String str) {
    return RegexHelper.dateFormat.hasMatch(str);
  }

  // validate name
  static dynamic validateFirstName({
    required BuildContext pageContext,
    required String value,
    bool useSnackbar = true,
    bool? isForDualScreen,
    double? foldAngle,
  }) {
    if (value.isEmpty) {
      if (useSnackbar) {
        ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
          title: 'register_failed_label'.tr,
          message: 'first_name_empty'.tr,
          status: false,
        ));
        return false;
      } else {
        return 'first_name_empty'.tr;
      }
    } else {
      if (useSnackbar) {
        return true;
      } else {
        return null;
      }
    }
  }

  static dynamic validateLastName({
    required BuildContext pageContext,
    required String value,
    bool useSnackbar = true,
    bool? isForDualScreen,
    double? foldAngle,
  }) {
    if (value.isEmpty) {
      if (useSnackbar) {
        ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
          title: 'register_failed_label'.tr,
          message: 'last_name_empty'.tr,
          status: false,
        ));
        return false;
      } else {
        return 'last_name_empty'.tr;
      }
    } else {
      if (useSnackbar) {
        return true;
      } else {
        return null;
      }
    }
  }

  // validate date
  static dynamic validateDate({
    required BuildContext pageContext,
    required String value,
    bool useSnackbar = true,
    bool? isForDualScreen,
    double? foldAngle,
  }) {
    if (value.isEmpty) {
      if (useSnackbar) {
        ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
          title: 'register_failed_label'.tr,
          message: 'dob_empty'.tr,
          status: false,
        ));
        return false;
      } else {
        return 'dob_empty'.tr;
      }
    }
    if (_isDate(value) == false) {
      if (useSnackbar) {
        ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
          title: 'register_failed_label'.tr,
          message: 'dob_invalid'.tr,
          status: false,
        ));
        return false;
      } else {
        return 'dob_invalid'.tr;
      }
    } else {
      if (useSnackbar) {
        return true;
      } else {
        return null;
      }
    }
  }

  // validate address
  static dynamic validateAddress({
    required BuildContext pageContext,
    required String value,
    bool useSnackbar = true,
    bool? isForDualScreen,
    double? foldAngle,
  }) {
    if (value.isEmpty) {
      if (useSnackbar) {
        ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
          title: 'register_failed_label'.tr,
          message: 'address_empty'.tr,
          status: false,
        ));
        return false;
      } else {
        return 'address_empty'.tr;
      }
    } else {
      if (useSnackbar) {
        return true;
      } else {
        return null;
      }
    }
  }
}
