import 'package:get/get.dart';
import '../modules/login/login_binding.dart';
import '../modules/login/login_view.dart';

import '../modules/register/register_view.dart';
import '../modules/splash/splash_binding.dart';
import '../modules/splash/splash_view.dart';
import '../modules/home/home_bindings.dart';
import '../modules/home/home_view.dart';

part 'app_routes.dart';

class AppPages{
  AppPages._();

  // ignore: constant_identifier_names
  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(name: _Paths.HOME, page: () =>
    const HomeView(),binding: HomeBinding(),
    ),
    GetPage(name: _Paths.SPLASH, page: () => const SplashView(),binding: SplashBinding(),),
    GetPage(name: _Paths.LOGIN, page: () => const LoginView(),binding: LoginBinding(),),
    GetPage(name: _Paths.REGISTER, page: () => const RegisterPage(),binding: LoginBinding(),),
    
  ];
}