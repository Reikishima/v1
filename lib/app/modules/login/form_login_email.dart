import 'package:flutter/material.dart';


import '../../../text/montserrat_text_view.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';


class FormLoginEmailAddress extends StatelessWidget {
  FormLoginEmailAddress({
    this.marginHorizontal,
    required this.emailAddressController,
    Key? key,
  }) : super(key: key);

  final double? marginHorizontal;

  final TextEditingController emailAddressController;
  final MontserratTextStyle montserratStyle = MontserratTextStyle();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(marginHorizontal ?? 10),
      ),
      child: TextField(
        controller: emailAddressController,
        keyboardType: TextInputType.emailAddress,
        cursorColor: AppColors.darkGold,
        style: montserratStyle.white5(),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(top: SizeConfig.vertical(2)),
          hintText: 'email address',
          hintStyle: montserratStyle.grey5(),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
