import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:get/get.dart';
// ignore: depend_on_referenced_packages
import 'package:responsive_framework/responsive_framework.dart';
import '../../../helpers/scroll_config.dart';
import '../../../layout/space_sizer.dart';
import '../../../layout/uner_lines.dart';
import '../../../text/montserrat_text_view.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';
import '../../../widget/buttons/custom_flat_buttons.dart';
import 'form_login_email.dart';
import 'login_conroller.dart';
import '../../../utils/app_colors.dart';
import '../../../frame/frame_scaffold.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return KeyboardSizeProvider(
      child: FrameScaffold(
        heightBar: 0,
        elevation: 0,
        avoidBottomInset: true,
        statusBarColor: AppColors.darkBackground,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        colorScaffold: AppColors.darkBackground,
        view: GetBuilder<LoginController>(
          init: LoginController(),
          builder: (LoginController controller) =>
              _annotatedWrapper(controller),
        ),
      ),
    );
  }

  Widget _annotatedWrapper(LoginController controller) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: Container(
        color: AppColors.darkBackground,
        child: Consumer<ScreenHeight>(
          builder: (BuildContext context, ScreenHeight res, Widget? child) =>
              _mainGestureWrapper(context, res, controller),
        ),
      ),
    );
  }

  Widget _mainGestureWrapper(
    BuildContext context,
    ScreenHeight res,
    LoginController signInController,
  ) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
        if (res.isSmall) {
          ScrollConfig.resetScrollView(signInController.scrollController);
        }
      },
      child: _mainViewWrapper(context, res, signInController),
    );
  }

  Widget _mainViewWrapper(
    BuildContext context,
    ScreenHeight res,
    LoginController signInController,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnMainAxisAlignment: MainAxisAlignment.center,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Expanded(
            child: CustomScrollView(
              controller: signInController.scrollController,
              physics: ScrollConfig.isUseScroll(res.isSmall),
              slivers: <Widget>[_sliver(context, signInController)],
            ),
          ),
        ),
      ],
    );
  }

  Widget _sliver(BuildContext context, LoginController signInController) {
    return SliverFillRemaining(
      hasScrollBody: false,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisAlignment: MainAxisAlignment.center,
        children: <ResponsiveRowColumnItem>[
          // logo header and text
          ResponsiveRowColumnItem(child: _logoApp()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 4)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'BLACK OWL',
              fontStyle: FontStyle.italic,
              size: SizeConfig.safeHorizontal(4),
              fontWeight: FontWeight.w300,
              color: Colors.white,
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 10)),
          // body section with form and login button
          ResponsiveRowColumnItem(
            child: _formWrapper(
              context: context,
              signInController: signInController,
            ),
          ),
          ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 4)),
          ResponsiveRowColumnItem(
            child: _loginButton(
              signInController: signInController,
              onTap: () {
                signInController.onValidateLogin(context);
              },
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          ResponsiveRowColumnItem(
            child: _versionNumber(signInController.versionApp.value ?? ''),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 10)),
        ],
      ),
    );
  }

  Widget _formWrapper({
    required BuildContext context,
    required LoginController signInController,
  }) {
    return FormLoginEmailAddress(
      emailAddressController: signInController.emailController,
    );
  }
}

Widget _underlineFormPhoneNumber() {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(10)),
    child: const Underline(),
  );
}

Container _loginButton({
  required Function() onTap,
  required LoginController signInController,
}) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(10)),
    width: SizeConfig.screenWidth,
    child: CustomFlatButton(
      onTap: onTap,
      text: 'Login'.tr,
      useCircularLoading: true,
      loading: signInController.isLoading.value,
    ),
  );
}

Widget _logoApp() {
  return Align(
    child: Image.asset(
      Assets.imageBlackOwlTransparent,
      width: SizeConfig.horizontal(45),
      height: SizeConfig.horizontal(45),
      fit: BoxFit.cover,
    ),
  );
}

Widget _versionNumber(String version) {
  return MontserratTextView(
    value: 'Version $version',
    color: AppColors.darkGold,
  );
}
          
        
                /*Image.asset('assets/images/logo.png',width: 100,height: 250,alignment: Alignment.center,),
                const SizedBox(
                  height: 20,
                ),
                const Text("Welcome in Wings Bird",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Color.fromARGB(255, 212, 158, 11))
                ),const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: InputDecoration(hintText: 'Masukkan Email dengan Benar!',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    labelText: "Email",
                    prefixIcon: const Icon(Icons.email),
                   
                  ),
                  keyboardType: TextInputType.emailAddress,
                  controller: controller.emailController,
                  onSaved: (value){
                    controller.email == value!;
                  },
                  validator: (value){
                    if(value !=null){
                    return 'isi email';
                  }
                  return controller.validateEmail(value!);
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                Obx(() => 
                TextFormField(
                  decoration: InputDecoration(hintText: 'Password Harus 4-6 Karakter',prefixIconColor: Color.fromARGB(130, 208, 127, 7),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    labelText: "Password",
                    prefixIcon: const Icon(Icons.lock),
                     suffix:  InkWell(child: const Icon(Icons.visibility,size: 20,),
                     onTap: () {
                       controller.isPasswordHidden.value =! controller.isPasswordHidden.value;
                     },
                     ), 
                    isDense: true
                  ),
                  obscureText: controller.isPasswordHidden.value,
                  controller: controller.passwordController,
                  onSaved: (value){
                    controller.password == value!;
                  },
                  validator: (value){
                    if(value !=null){
                   return 'isi password' ;
                    }
                    return controller.validatePassword(value!);
                  },
                  
                ),
                ),
                const SizedBox(
                  height: 10,
                ),
                ConstrainedBox(constraints: BoxConstraints.tightFor(width: context.width,),
                child: ElevatedButton(
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    backgroundColor: MaterialStateProperty.all(AppColors.darkBackground),padding: MaterialStateProperty.all(const EdgeInsets.all(14))
                  ),
                  child: const Text('Login',style: TextStyle(fontSize: 14,color : Color.fromARGB(255, 235, 136, 6),),
                  ),
                  onPressed: () {
                    controller.checkLogin();
                    
                  },
                  ))*/

              

 