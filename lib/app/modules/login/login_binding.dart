import 'package:get/get.dart';
import 'package:v1/app/modules/login/login_conroller.dart';


class LoginBinding extends Bindings{
  @override 
  void dependencies(){
    Get.lazyPut<LoginController>(() => LoginController());
  }
}