// ignore: depend_on_referenced_packages
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static final List<String> imgSlider = [
    'blackowl_transparent.png',
    'logo_bo.png'
        'Logo.png'
  ];
  final CarouselSlider autoPlayImage = CarouselSlider(
    options: CarouselOptions(autoPlay: true, height: 20),
    items: imgSlider.map((fileImage) {
      return Container(
        margin: const EdgeInsets.all(0.0),
        child: ClipRRect(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          child: Image.asset(
            'assets/images/$fileImage',
            width: 10,
            height: 10,
            fit: BoxFit.cover,
          ),
        ),
      );
    }).toList(),
  );

  @override
  Widget build(BuildContext context) {
    
    throw UnimplementedError();
  }
}
