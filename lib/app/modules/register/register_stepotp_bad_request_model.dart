// ignore_for_file: sort_constructors_first

// ignore: depend_on_referenced_packages
import 'package:json_annotation/json_annotation.dart';

/// This allows the `RegisterStepOtpBadRequestModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'register_step_otp_bad_request_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class RegisterStepOtpBadRequestModel {
  // constructor
  RegisterStepOtpBadRequestModel({this.message, this.error});

  // variable parameters
  @JsonKey(name: 'message')
  final String? message;
  @JsonKey(name: 'error')
  final Error? error;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$RegisterStepOtpBadRequestModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory RegisterStepOtpBadRequestModel.fromJson(Map<String, dynamic> json) =>
      _$RegisterStepOtpBadRequestModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$RegisterStepOtpBadRequestModelToJson`.
  Map<String, dynamic> toJson() => _$RegisterStepOtpBadRequestModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Error {
  // constructor
  Error({this.code, this.message});

  // variable parameters
  @JsonKey(name: 'code')
  final int? code;
  @JsonKey(name: 'message')
  final Message? message;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$ErrorFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Error.fromJson(Map<String, dynamic> json) => _$ErrorFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$ErrorToJson`.
  Map<String, dynamic> toJson() => _$ErrorToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Message {
  // constructor
  Message({
    this.countryCode,
    this.phone,
    this.email,
    this.firstName,
    this.lastName,
    this.dob,
    this.gender,
    this.cityId,
    this.address,
  });

  // variable parameters
  @JsonKey(name: 'country_code')
  final List<String>? countryCode;
  @JsonKey(name: 'phone')
  final List<String>? phone;
  @JsonKey(name: 'email')
  final List<String>? email;
  @JsonKey(name: 'first_name')
  final List<String>? firstName;
  @JsonKey(name: 'last_name')
  final List<String>? lastName;
  @JsonKey(name: 'dob')
  final List<String>? dob;
  @JsonKey(name: 'gender')
  final List<String>? gender;
  @JsonKey(name: 'city_id')
  final List<String>? cityId;
  @JsonKey(name: 'address')
  final List<String>? address;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MessageFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MessageToJson`.
  Map<String, dynamic> toJson() => _$MessageToJson(this);
}
