import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import 'package:v1/app/modules/home/home_view.dart';


class RegisterController extends GetxController {
  //constructor
  RegisterController({required this.pageContext});
  final BuildContext pageContext;
  final loginFormKey = GlobalKey<FormState>();
  //scroll controller
  ScrollController scrollController = ScrollController();
  //contrroller textfield
  TextEditingController firstname = TextEditingController();
  TextEditingController lastname = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController username = TextEditingController();

  //selected date
  Rx<DateTime> selectedDate = DateTime.now().obs;

  //initiate variable
  RxString firstnameErrorMessage = RxString('');
  RxString lastnameErrorMessage = RxString('');
  RxString addressnameErrorMessage = RxString('');
  RxString emailnameErrorMessage = RxString('');
  RxBool isEmailAddressReadOnly = RxBool(false);
  RxString firstNameErrorMessage = RxString('');
  RxString lastNameErrorMessage = RxString('');
  RxString phoneNumberErrorMessage = RxString('');
  RxString emailAddressErrorMessage = RxString('');
  RxString addressErrorMessage = RxString('');
  RxString cityErrorMessage = RxString('');
  /*TextEditingController? emailController,passwordController;
final email = '';
final password= '';
var isPasswordHidden=true.obs;*/


  dynamic onChangeDatePicker(dynamic value) {
    selectedDate.value = value as DateTime;
  }

  /*bool _firstNameError(){
      String? message;
      final dynamic temp = Validation.validateFirstName(
        pageContext: pageContext,
        value: firstname.text,
        useSnackbar: false,
      );
      message = temp is String ? temp : null;
      firstnameErrorMessage.value = message ?? '';
      //me return jika benar
      if(message !=null){
        return false;
      }else{
        return true;
      }
  }
  bool _lastNameError(){
      String? message;
      final dynamic temp = Validation.validateLastName(
        pageContext: pageContext,
        value: lastname.text,
        useSnackbar: false,
      );
      message = temp is String ? temp : null;
      lastnameErrorMessage.value = message ?? '';
      //me return jika benar
      if(message !=null){
        return false;
      }else{
        return true;
      }
  }
  bool _addressError(){
      String? message ;
      final dynamic temp = Validation.validateAddress(
        pageContext: pageContext,
        value: username.text,
        useSnackbar: false,
      );
      message = temp is String ? temp : null;
      addressnameErrorMessage.value = message ?? '';
      //me return jika benar
      if(message == null){
        return false;
      }else{
        return true;
      }
  }
  String _convertDateofBirth(){
    return DateHelper.getBirthOfDateSender(selectedDate.toString());
  }
  dynamic _resetValue() {
    firstNameErrorMessage.value = '';
    lastNameErrorMessage.value = '';
    phoneNumberErrorMessage.value = '';
    emailAddressErrorMessage.value = '';
    addressErrorMessage.value = '';
  }*/
  dynamic onPressRegister() {
    //validate
    /* final bool isValidatesFirstName = _firstNameError();
    final bool isValidatesLastName =  _lastNameError();
    final bool isValidatesaddress = _addressError();*/
    Get.off(const HomeView());
  }
  /*dynamic _whenReceiveRegisterValidate(Map<String, dynamic> param) {
    final RegisterStepOtpBadRequestModel dataBadRequest =
        RegisterStepOtpBadRequestModel.fromJson(param);
    // error message for first name
    firstNameErrorMessage.value =
        dataBadRequest.error?.message?.firstName?[0] ?? '';
    // error message for first name
    lastNameErrorMessage.value =5
        dataBadRequest.error?.message?.lastName?[0] ?? '';
    // error message for email adddress
    emailAddressErrorMessage.value =
        dataBadRequest.error?.message?.email?[0] ?? '';
    // error message for address
    addressErrorMessage.value =
        dataBadRequest.error?.message?.address?[0] ?? '';
  }*/



  /* String? validator (String value){
    if(value.isEmpty){
      return 'Please this Field must be filled';
    }
    return null;
  }

  validateEmail(value){
    if(!GetUtils.isEmail(value)){
      return 'bukti email valid';
    }
    return null;
  }
 validatePassword(value){
    if(value.isEmpty){
      return 'Password harus 4- 6 karakter';
    }else if(value.lenght > 7){
      return 'too much';
    }
    return null;
  }

  void checkLogin(){
      if (emailController != email && passwordController !=password ){
        Get.off(const LoginView());
      }else{
        Get.put(LoginController());
      }
  }
  
  }*/
}
