// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_stepotp_bad_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterStepOtpBadRequestModel _$RegisterStepOtpBadRequestModelFromJson(
        Map<String, dynamic> json) =>
    RegisterStepOtpBadRequestModel(
      message: json['message'] as String?,
      error: json['error'] == null
          ? null
          : Error.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RegisterStepOtpBadRequestModelToJson(
        RegisterStepOtpBadRequestModel instance) =>
    <String, dynamic>{
      'message': instance.message,
      'error': instance.error,
    };

Error _$ErrorFromJson(Map<String, dynamic> json) => Error(
      code: json['code'] as int?,
      message: json['message'] == null
          ? null
          : Message.fromJson(json['message'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ErrorToJson(Error instance) => <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
    };

Message _$MessageFromJson(Map<String, dynamic> json) => Message(
      countryCode: (json['country_code'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      phone:
          (json['phone'] as List<dynamic>?)?.map((e) => e as String).toList(),
      email:
          (json['email'] as List<dynamic>?)?.map((e) => e as String).toList(),
      firstName: (json['first_name'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      lastName: (json['last_name'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      dob: (json['dob'] as List<dynamic>?)?.map((e) => e as String).toList(),
      gender:
          (json['gender'] as List<dynamic>?)?.map((e) => e as String).toList(),
      cityId:
          (json['city_id'] as List<dynamic>?)?.map((e) => e as String).toList(),
      address:
          (json['address'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'country_code': instance.countryCode,
      'phone': instance.phone,
      'email': instance.email,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'dob': instance.dob,
      'gender': instance.gender,
      'city_id': instance.cityId,
      'address': instance.address,
    };
