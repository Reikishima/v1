import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:get/get.dart';

import 'package:v1/app/modules/home/home_view.dart';
import 'package:v1/app/modules/login/login_view.dart';

import 'package:v1/utils/app_colors.dart';
// ignore: depend_on_referenced_packages


import 'package:intl/intl.dart';


class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController dateInput = TextEditingController();
  List<String> list = [
    'Jakarta',
    'Bandung',
    'Jawa Teangah',
    'Jawa Timur',
    'Banten'
  ];
  List<String> kota = ['CDE', 'FGH', 'BCD', 'ABC', 'Jakarta'];
  String? data;
  String? city;
  DateTime selectedDate = DateTime.now();
  TextEditingController dateController = TextEditingController();
  int? _value = 1;
  bool passwordVisible = false;
  bool passwordConfirmVisible = false;
  void togglePassword() {
    setState(() {
      passwordVisible = !passwordVisible;
    });
  }

  @override
  void initState() {
    dateInput.text = '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.darkBackground,
          title: const Text(
            'Register',
            style: TextStyle(color: Color.fromARGB(255, 218, 197, 134)),
          ),
          actions: [
            IconButton(
              icon: Image.asset(
                'assets/images/blackowl-transparent.png',
                height: 100,
                width: 100,
              ),
              onPressed: () {},
            )
          ],
          leading: IconButton(
            onPressed: () {
              Get.to(const LoginView());
            },
            icon: const Icon(Icons.arrow_back_ios_new),
            color: AppColors.flatGold,
          ),
        ),
        backgroundColor: AppColors.darkBackground,
        body: SafeArea(
          child: Container(
            padding: const EdgeInsets.all(20),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: 'First Name',
                      labelStyle: TextStyle(color: AppColors.darkGold),
                      hintText: 'fill your First Name',
                      fillColor: AppColors.darkBackground,
                      hintStyle: TextStyle(
                          color: AppColors.greyDisabled, fontSize: 12),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: AppColors.darkGold),
                      ),
                    ),
                    style: TextStyle(color: AppColors.flatGold),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Last Name',
                      labelStyle: TextStyle(color: AppColors.darkGold),
                      hintText: 'fill your Last Name',
                      fillColor: AppColors.darkBackground,
                      hintStyle: TextStyle(
                          color: AppColors.greyDisabled, fontSize: 12),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: AppColors.darkGold),
                      ),
                    ),
                    style: TextStyle(color: AppColors.flatGold),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Phone Number',
                      labelStyle: TextStyle(color: AppColors.darkGold),
                      hintText: 'fill your phonenumber',
                      fillColor: AppColors.darkBackground,
                      hintStyle: TextStyle(
                          color: AppColors.greyDisabled, fontSize: 12),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: AppColors.darkGold),
                      ),
                    ),
                    style: TextStyle(color: AppColors.flatGold),
                  ),
                 TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Email Address',
                      labelStyle: TextStyle(color: AppColors.darkGold),
                      hintText: 'fill your email address',
                      fillColor: AppColors.darkBackground,
                      hintStyle: TextStyle(
                          color: AppColors.greyDisabled, fontSize: 12),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: AppColors.darkGold),
                      ),
                    ),
                    style: TextStyle(color: AppColors.flatGold,fontSize: 14,fontFamily: 'Roboto',fontWeight: FontWeight.w300),
                  ),
                  TextField(
                    controller: dateInput,
                    decoration: InputDecoration(
                      fillColor: AppColors.darkFlatGold,
                      labelText: 'Masukkan Tanggal',
                      labelStyle: TextStyle(color: AppColors.darkGold),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: AppColors.darkGold),
                      ),
                    ),
                    readOnly: true,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(1945),
                          lastDate: DateTime(2100));
                      if (pickedDate != null) {
                        String formattedDate = DateFormat(
                          'yyyy-MM-dd',
                        ).format(pickedDate);

                        setState(() {
                          dateInput.text = formattedDate;
                        });
                      } else {}
                    },
                    style: TextStyle(color: AppColors.flatGold),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 10,
                    ),
                    child: Row(
                      children: [
                        Text(
                          'Pilih Gender',
                          style: TextStyle(color: AppColors.darkGold),
                        )
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Male',
                        style: TextStyle(color: AppColors.flatGold),
                      ),
                      Radio(
                          value: 1,
                          activeColor: AppColors.darkGold,
                          groupValue: _value,
                          onChanged: (value) {
                            setState(() {
                              // ignore: unnecessary_cast
                              _value = value as int?;
                            });
                          }),
                      Text(
                        'Female',
                        style: TextStyle(color: AppColors.flatGold),
                      ),
                      Radio(
                          value: 2,
                          activeColor: AppColors.darkGold,
                          groupValue: _value,
                          onChanged: (value) {
                            setState(() {
                              // ignore: unnecessary_cast
                              _value = value as int?;
                            });
                          })
                    ],
                  ),
                  //DropDownButton Province
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      DropdownButton<String>(
                          focusColor: AppColors.flatGold,
                          style: TextStyle(color: AppColors.flatGold),
                          dropdownColor: AppColors.darkBackground,
                          hint: Text(
                            'Province',
                            style: TextStyle(color: AppColors.flatGold),
                          ),
                          value: data,
                          onChanged: (newValue) {
                            setState(() {
                              data = newValue;
                            });
                          },
                          items: list.map((lust) {
                            return DropdownMenuItem(
                              value: lust,
                              child: Text(lust),
                            );
                          }).toList()),
                      DropdownButton<String>(
                          focusColor: AppColors.flatGold,
                          style: TextStyle(color: AppColors.flatGold),
                          dropdownColor: AppColors.darkBackground,
                          hint: Text(
                            'City',
                            style: TextStyle(color: AppColors.flatGold),
                          ),
                          value: city,
                          onChanged: (newValue) {
                            setState(() {
                              city = newValue;
                            });
                          },
                          items: kota.map((lust) {
                            return DropdownMenuItem(
                              value: lust,
                              child: Text(lust),
                            );
                          }).toList()),
                    ],
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Address',
                      labelStyle: TextStyle(color: AppColors.darkGold),
                      hintText: 'fill your Address',
                      fillColor: AppColors.darkBackground,
                      hintStyle: TextStyle(
                          color: AppColors.greyDisabled, fontSize: 12),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: AppColors.darkGold),
                      ),
                    ),
                    style: TextStyle(color: AppColors.flatGold),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: 300,
                      child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: AppColors.darkGold,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                          ),
                          onPressed: () {
                            Get.off(const HomeView());
                          },
                          child: const Center(
                            child: Text(
                              'Register',
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                          ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        ); 
        /*Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 0),
          child: SingleChildScrollView(
            child: Form(
              child: Column(children: [
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'FirstName',
                    fillColor: AppColors.darkBackground,
                    contentPadding: const EdgeInsets.only(left: 35),
                    hintStyle: heading6.copyWith(color: AppColors.flatGold),
                    border: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                    ),
                  ),
                  style: TextStyle(color: AppColors.flatGold),
                ),
                ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
                const ResponsiveRowColumnItem(
                  child: SpaceSizer(
                    vertical: 2,
                  ),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Last Name',
                    fillColor: AppColors.greenInfo,
                    contentPadding: const EdgeInsets.only(left: 35),
                    hintStyle: heading6.copyWith(color: AppColors.flatGold),
                    border:
                        const OutlineInputBorder(borderSide: BorderSide.none),
                  ),
                  style: TextStyle(color: AppColors.flatGold),
                ),
                ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
                const ResponsiveRowColumnItem(
                  child: SpaceSizer(
                    vertical: 2,
                  ),
                ),
                TextFormField(
                  obscureText: !passwordConfirmVisible,
                  decoration: InputDecoration(
                    hintText: 'Phone Number',
                    fillColor: AppColors.darkGold,
                    contentPadding: const EdgeInsets.only(left: 35),
                    hintStyle: heading6.copyWith(color: AppColors.flatGold),
                  ),
                  style: TextStyle(color: AppColors.flatGold),
                ),
                ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
                const ResponsiveRowColumnItem(
                  child: SpaceSizer(
                    vertical: 2,
                  ),
                ),
                TextFormField(
                  decoration: InputDecoration(
                      hintText: 'EmailAdress',
                      fillColor: AppColors.darkBackground,
                      contentPadding: const EdgeInsets.only(left: 35),
                      hintStyle: heading6.copyWith(color: AppColors.flatGold),
                      border: const OutlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                      ),
                      style: TextStyle(color: AppColors.flatGold),
                ),
                ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
                const ResponsiveRowColumnItem(
                  child: SpaceSizer(
                    vertical: 2,
                  ),
                ),
                TextFormField(
                  decoration: InputDecoration(
                      hintText: 'Adress',
                      fillColor: AppColors.darkBackground,
                      contentPadding: const EdgeInsets.only(left: 35),
                      hintStyle: heading6.copyWith(color: AppColors.flatGold),
                      border: const OutlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                      ),
                      style: TextStyle(color: AppColors.flatGold),
                ),
                ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
                const ResponsiveRowColumnItem(
                  child: SpaceSizer(
                    vertical: 2,
                  ),
                ),

                /* Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                      child: Text(
                        selectedDate.toString(),
                      ),
                    ),
                    OutlinedButton(
                      onPressed: (){
                        showDatePicker(context: context, initialDate: selectedDate, 
                        firstDate: DateTime(1945), lastDate: DateTime(2101),
                        ).then((value){
                          if(value != null) {
                            setState(() {
                              selectedDate = value;
                            });
                          }
                        });
                      },
                      child: const Text('DATE PICKER'),
                ),
                ]),
                      ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
                   const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2,),),*/

                Container(
                  alignment: Alignment.bottomLeft,
                  child: TextField(
                    controller: dateInput,
                    decoration: InputDecoration(
                      icon: const Icon(Icons.calendar_today,),iconColor: AppColors.flatGold,
                      labelText: 'Masukkan Data',labelStyle: TextStyle(color: AppColors.flatGold)
                    ),
                    readOnly: true,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(1945),
                          lastDate: DateTime(2100));
                      if (pickedDate != null) {
                       
                        String formattedDate = DateFormat(
                          'yyyy-MM-dd',
                        ).format(pickedDate);
                        
                        setState(() {
                          dateInput.text = formattedDate;
                        });
                      } else {}
                    },
                    style: TextStyle(color: AppColors.flatGold),
                  ),
                  
                ),
                
                ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
                const ResponsiveRowColumnItem(
                  child: SpaceSizer(
                    vertical: 2,
                  ),
                ),
                Column(children: [
                  Text(
                    "pilih Gender",
                    textAlign: TextAlign.left,
                    style: TextStyle(color: AppColors.flatGold),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 60,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: AppColors.darkBackground,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 20, left: 20, bottom: 20, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Male',
                            style: TextStyle(color: AppColors.flatGold),
                          ),
                          Radio(
                              value: 1,activeColor: AppColors.darkGold,
                              groupValue: _value,
                              onChanged: (value) {
                                setState(() {
                                  // ignore: unnecessary_cast
                                  _value = value as int?;
                                });
                              }),
                          Text(
                            'Female',
                            style: TextStyle(color: AppColors.flatGold),
                          ),
                          Radio(
                              value: 2,activeColor: AppColors.darkGold,
                              groupValue: _value,
                              onChanged: (value) {
                                setState(() {
                                  // ignore: unnecessary_cast
                                  _value = value as int?;
                                });
                              })
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                      left: 10,
                    ),
                    alignment: Alignment.bottomLeft,
                    child: DropdownButton<String>(focusColor: AppColors.flatGold,style: TextStyle(color: AppColors.flatGold),dropdownColor: AppColors.darkBackground,
                        hint:  Text('Province',style: TextStyle(color: AppColors.flatGold),),
                        value: data,
                        onChanged: (newValue) {
                          setState(() {
                            data = newValue;
                          });
                        },
                        items: list.map((lust) {
                          return DropdownMenuItem(
                            value: lust,
                            child: Text(lust),
                          );
                          
                        }
                        ).toList()),
                  ),
                  /* DropdownSearch<String>(
                          items: const ['Jakarta','Jawa Tengah','Jawa Timur','Banten'],
                          onChanged: print,selectedItem: 'Banten',
              
                        ),*/
                  SizedBox(
                    width: 300,
                    child: TextButton(
                      style: TextButton.styleFrom(
                        backgroundColor: AppColors.darkGold,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                      ),
                      onPressed: () {
                        Get.off(const HomeView());
                      },
                      child: const Center(
                        child: Text(
                          'Register',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                ]),
              ]),

              /*TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Gender',fillColor: AppColors.darkBackground,
                          hintStyle: heading6.copyWith(color: AppColors.flatGold),
                          border: const OutlineInputBorder(
                            borderSide: BorderSide.none,
                          )
                        ),
                      ),
                      ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
                   const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2,),),*/

              /*TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Gender',fillColor: AppColors.darkBackground,
                          hintStyle: heading6.copyWith(color: AppColors.flatGold),
                          border: const OutlineInputBorder(
                            borderSide: BorderSide.none,
                          )
                        ),
                      ),*/

              /*const CustomPrimaryButton(
                buttonColor:Color.fromARGB(252, 19, 18, 15),
                textValue: 'Register',
                textColor: Colors.white,
              ),*/
            ),
          ),
        )));*/
  }
}


/*const SizedBox(height: 32,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const CustomCheckbox(),
                  const SizedBox(height: 80,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'By creating an account, you agree to our',
                        style: regular16pt.copyWith(color: textGrey),
                      ),
                      Text(
                        'Term & Condition',
                        style: regular16pt.copyWith(color: AppColors.darkGold),
                      )
                    ],
                  )
                ],
              ),*/

 /*const SizedBox(height: 1,),
                    TextFormField(
                        obscureText: !passwordConfirmVisible,
                        decoration: InputDecoration(
                          hintText: '  Phone Number',fillColor: AppColors.darkGold,
                          hintStyle: heading6.copyWith(color: AppColors.darkGold),
                          suffixIcon: IconButton(
                            color: AppColors.darkGold,
                            icon: Icon(passwordConfirmVisible ? Icons.visibility_outlined : Icons.voice_over_off_outlined),
                            onPressed: () {
                              setState(() {
                                passwordConfirmVisible = !passwordConfirmVisible;
                              });
                            }, 
                          ),
                          
                        ),
                        
                      ),
                      ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
                   const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
                    
                  ],
                  
                )
                
              ),*/
             /* const SizedBox(height: 1,),
                    TextFormField(
                        obscureText: !passwordVisible,
                        decoration: InputDecoration(
                          hintText: 'Last Name',
                          hintStyle: heading6.copyWith(color: AppColors.darkGold),
                          suffixIcon: IconButton(
                            color: AppColors.darkGold,
                            splashRadius: 1,
                            icon: Icon(passwordVisible ? Icons.visibility_outlined : Icons.visibility_off_outlined),
                            onPressed: togglePassword,
                          ),
                          border: const OutlineInputBorder(borderSide: BorderSide.none)
                        ),
                      ),
                      ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
                   const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),*/
