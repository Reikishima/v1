import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:google_fonts/google_fonts.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import 'ripple_buttons.dart';

class CustomFlatButton extends StatelessWidget {
  const CustomFlatButton({
    Key? key,
    required this.onTap,
    required this.text,
    this.textColor,
    this.textColorLoading,
    this.textSize,
    this.backgroundColor,
    this.height,
    this.width,
    this.image,
    this.radius,
    this.borderColor,
    this.loading = false,
    this.useCircularLoading = false,
  }) : super(key: key);

  final VoidCallback onTap;
  final String text;
  final Color? textColor;
  final Color? textColorLoading;
  final double? textSize;
  final Color? backgroundColor;
  final double? height;
  final double? width;
  final String? image;
  final double? radius;
  final Color? borderColor;
  final bool loading;
  final bool useCircularLoading;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: width ?? SizeConfig.horizontal(80),
      height: height ?? SizeConfig.vertical(5),
      decoration: BoxDecoration(
        border: Border.all(
          color: borderColor ?? backgroundColor ?? AppColors.flatGold,
        ),
        borderRadius: BorderRadius.circular(
          SizeConfig.horizontal(radius ?? 5),
        ),
        color: loading
            ? AppColors.greyDisabled
            : backgroundColor ?? AppColors.flatGold,
      ),
      child: RippleButton(
        radius: radius ?? 10,
        onTap: () {
          loading ? _emptyAction() : onTap();
        },
        child: _rowWrapper(),
      ),
    );
  }

  Widget _rowWrapper() {
    if (useCircularLoading == true && loading == true) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: SizeConfig.horizontal(5),
            height: SizeConfig.horizontal(5),
            child: CircularProgressIndicator(color: AppColors.whiteSkeleton),
          ),
        ],
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _builderWrapper(),
          Text(
            text,
            style: GoogleFonts.montserrat(
              fontWeight: FontWeight.w700,
              fontSize: SizeConfig.safeHorizontal(textSize ?? 4.5),
              color: loading
                  ? textColorLoading ?? AppColors.whiteBackground
                  : textColor ?? AppColors.darkBackground,
            ),
          ),
        ],
      );
    }
  }

  Widget _builderWrapper() {
    return Builder(
      builder: (_) {
        if (image != null) {
          return Container(
            margin: EdgeInsets.only(right: SizeConfig.horizontal(2)),
            child: Image.asset(image!, height: SizeConfig.vertical(4)),
          );
        } else {
          return Container();
        }
      },
    );
  }

  dynamic _emptyAction() {
    return null;
  }
}
